# MTProto proxy server docker-compose

With docker-compose file you can easy install MTProto proxy telegram and quickly configure it.

### Install Docker and Docker-Compose
If you don`t have docker and docker compose, see official documentation:
 * [Docker CE install](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
 
    Quick guide how to install Docker CE on Ubuntu:
    ```
    $ sudo apt-get update
    $ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    $ sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
     $ sudo apt-get update
     $ sudo apt-get install docker-ce
    ```
 * [Docker-Compose install](https://docs.docker.com/compose/install/)
    
    Quick guide how to install Docker-Compose on Ubuntu:
    ```
    $ sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    $ sudo chmod +x /usr/local/bin/docker-compose
    ```

# Clone repository
```
$ git clone git@gitlab.com:aleksei-g/mtproto-docker-compose.git
```

# Edit config.env
In that file you can configure:
 * TAG Value for promote chanel
 * Preset Secret, up to 16
 * Secret count for generate, up to 16
 * Workers count

# Configure auto update image
The development team recommends update and restart the container about once a day.
To implement this scenario, use the script *update*.

Sequencing:
 * Make script file executable: in project path run:
   ```
   $ chmod +x update
   ```
 * Create a task in crone. Run:
   ```
   $ sudo crontab -e
   ```
   and past in the end of file string:
   ```
   0 0 * * * /path/to/project/update
   ```
   The task will be executed daily at 0:00.
   
# Start proxy
In project path run:
```
$ sudo docker-compose up -d
```

# Stop proxy
In project path run:
```
$ sudo docker-compose down
```

# Get logs and connections info
To view information about the secret key and special links, look at the container's log using the command:
```
$ sudo docker-compose logs -f  --tail=30 mtproxy
```
The MTProto-proxy server presents some statistics on its work. Statistics available only on localhost:
```
$ sudo docker exec mtproxy.local curl http://localhost:2398/stats
```

# Additional info
By default, the MTProto-proxy server works on 443 port. Make sure it is open.
